package test.inacap.recetas;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;



public class MainActivity extends AppCompatActivity {

    private TextView tvNombre;
    private Button buscar;
    private TextView mostrar;
    private TextView mostrar2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.tvNombre = (TextView) findViewById(R.id.nombre);
        this.buscar = (Button) findViewById(R.id.bt_Buscar);
        this.mostrar = (TextView) findViewById(R.id.tv_Mostrar);
        this.mostrar2 = (TextView) findViewById(R.id.tv_Mostrar2);

        this.buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String x = tvNombre.getText().toString();
                String url = "https://test-es.edamam.com/search?q=" + x + "&app_id=a521b380&app_key=c2c929fcf293fdf400995825d8f2403f&from=0&to=3&calories=591-722&health=alcohol-free";

                StringRequest solicitud = new StringRequest(
                    Request.Method.GET,
                            url,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        JSONObject respuestaJSON = new JSONObject(response);
                                        JSONArray hits = respuestaJSON.getJSONArray("hits");
                                        JSONObject objeto1 = hits.getJSONObject(0);
                                        JSONObject recipe = objeto1.getJSONObject("recipe");
                                        JSONArray ingredients = recipe.getJSONArray("ingredientLines");
                                        JSONObject infoN = recipe.getJSONObject("totalNutrients");
                                        JSONObject energyK = infoN.getJSONObject("ENERC_KCAL");
                                        JSONObject sugar = infoN.getJSONObject("SUGAR");
                                        JSONObject protein = infoN.getJSONObject("PROCNT");
                                        String m = "";
                                        for(int i=0; i<ingredients.length(); i++){
                                            m = m + "- " + ingredients.getString(i) + "\n";
                                        }
                                        mostrar.setText(m);
                                        mostrar2.setText(
                                                energyK.getString("label")+"\t\t"+energyK.getInt("quantity")+" "+energyK.getString("unit")
                                        +"\n"+sugar.getString("label")+"\t\t"+sugar.getInt("quantity")+" "+sugar.getString("unit")
                                        +"\n"+protein.getString("label")+"\t\t"+protein.getInt("quantity")+" "+protein.getString("unit"));


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // e
                                }

                }
                    );
                RequestQueue listaEspera = Volley.newRequestQueue(getApplicationContext());
                listaEspera.add(solicitud);

            }
        });



}}
